--- Comandos útiles en Ubunto para usar SQL Server
sudo wget -qO- http://150.136.51.105/static/dist/carga_servicios.sql.gz
sqlcmd -S localhost -U SA
sqlcmd -S localhost -U SA -P Tamarilla1 -i carga_servicios_ene.sql -o salida.txt

--- Consultas útiles generales
SELECT @@VERSION
SELECT name FROM master.dbo.sysdatabases
SELECT name FROM sys.tables
SELECT COUNT(*) FROM TBLPROMEDIOS_FESTIVOS

--- Opraciones sobre la BD
CREATE DATABASE AnaliticaMoviva
USE AnaliticaMoviva
ALTER DATABASE [AnaliticaMoviva] MODIFY FILE ( NAME = N'AnaliticaMoviva_log', MAXSIZE = UNLIMITED)
