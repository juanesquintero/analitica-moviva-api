SELECT TOP (1000) D.INTIDSERVICIODETALLE,CONVERT(DATE, D.DTMFECHAINGRESO) AS DIA,
datepart(hh, D.DTMFECHAINGRESO)  AS HORA, DR.INTCX, DR.INTCY
FROM TBLSERVICIODETALLEHST D
INNER JOIN TBLCLIENTE C ON D.INTIDCLIENTE=C.INTIDCLIENTE
INNER JOIN TBLDIRECCIONCLIENTE DC ON C.INTIDCLIENTE=DC.INTIDCLIENTE
INNER JOIN TBLDIRECCION DR ON DC.INTIDDIRECCION=DR.INTIDDIRECCION
WHERE CONVERT(DATE, D.DTMFECHAINGRESO) >= '2021-01-01'
AND CONVERT(DATE, D.DTMFECHAINGRESO) < '2021-02-01'
GROUP BY D.INTIDSERVICIODETALLE,CONVERT(DATE, D.DTMFECHAINGRESO),datepart(hh,D.DTMFECHAINGRESO),
DR.INTCX,DR.INTCY
ORDER BY D.INTIDSERVICIODETALLE,CONVERT(DATE,D.DTMFECHAINGRESO),datepart(hh,D.DTMFECHAINGRESO)

SELECT COUNT(*)
FROM TBLSERVICIODETALLEHST D
INNER JOIN TBLCLIENTE C ON D.INTIDCLIENTE=C.INTIDCLIENTE
INNER JOIN TBLDIRECCIONCLIENTE DC ON C.INTIDCLIENTE=DC.INTIDCLIENTE
INNER JOIN TBLDIRECCION DR ON DC.INTIDDIRECCION=DR.INTIDDIRECCION
WHERE CONVERT(DATE, D.DTMFECHAINGRESO) >= '2021-01-01'
AND CONVERT(DATE, D.DTMFECHAINGRESO) < '2021-02-01'

SELECT INTORDEN ORDEN,DTMFECHA FROM TBLDIAS_FESTIVOS
        WHERE DTMFECHA >= '2021-08-01' AND DTMFECHA < '2021-09-01'

SELECT * FROM TBLENTRENAMIENTOS
