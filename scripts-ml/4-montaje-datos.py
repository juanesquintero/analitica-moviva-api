from sqlalchemy import create_engine

# Conectar la base de datos del proyecto Moviva
con_uri = 'mssql+pyodbc://SA:Tamarilla1@35.193.216.104:1433/AnaliticaMoviva'
con_uri += '?driver=ODBC+Driver+17+for+SQL+Server'
engine = create_engine(con_uri, fast_executemany=True, connect_args={'connect_timeout': 10}, echo=False)

fic = open('carga_servicios_ene.sql', "r")
lines = fic.readlines()
fic.close()
connection = engine.raw_connection()
cursor = connection.cursor()
for command in lines:
    cursor.execute(command)
connection.commit()
cursor.close()
engine.dispose()