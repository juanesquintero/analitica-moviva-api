import pandas as pd

# Cargar los feriados en Colombia entre el 2017 y el 2022
file = 'Feriados Colombia.csv'
headers = ['orden','nombre', 'fecha', 'periodo', 'dia_nombre'] 
dtypes = {'orden': 'int', 'nombre': 'str', 'fecha': 'str', 'periodo': 'int', 'dia_nombre': 'str'}
f = pd.read_csv(file, header=None, names=headers, dtype=dtypes, parse_dates=['fecha'],encoding="ISO-8859-1")

# Cargar el conteo de servicios para cada fecha, hora , latitud y longitud 
file = 'Servicios Conteo X Hora con Fecha.csv'
headers = ['fecha', 'periodo', 'semana', 'dia_nombre', 'hora', 'lat','lon', 'conteo'] 
dtypes = {'fecha': 'str', 'periodo': 'int', 'semana': 'int', 'dia_nombre': 'str',
          'hora': 'int', 'lat': 'float', 'lon': 'float', 'conteo': 'int'}
s = pd.read_csv(file, header=None, names=headers, dtype=dtypes, parse_dates=['fecha'])

# Reemplazar el periodo con el peso de cada periodo para la ponderación
s['periodo'].replace([2017,2018,2019],[0.2,0.3,0.5],inplace=True)
s['ponderacion'] = s.periodo * s.conteo

# Determinar para cada servicio si se realizó en un día feriado o no
s['feriado'] = s.fecha.isin(f.fecha)
s.lat = round(s.lat,3)
s.lon = round(s.lon,3)

# Separar el DataFrame en dos: uno para días ordinarios y otro para feriados
ordi = s[s.feriado==False]
feri_aux = s[s.feriado==True]

# Extraer el orden del feriado para los servicios realizados en días feriados
# Nota: el 2019-07-01 se celebraron dos feriados, se debe eliminar uno en f para evitar duplicados
feri = feri_aux.merge(f, left_on='fecha', right_on='fecha')

# Agrupar para sumar las ponderaciones y obtener el Promedio Móvil Ponderado
po = ordi.groupby(['semana','dia_nombre','hora','lat','lon']).ponderacion.sum()
pf = feri.groupby(['orden','hora','lat','lon']).ponderacion.sum()

po.to_csv('Servicios Promedio X Hora - Ordinario.csv', float_format='%.3f')
pf.to_csv('Servicios Promedio X Hora - Feriados.csv', float_format='%.3f')

# Para filtar filas y columnas: df = df [condición][['col1','col2', ...]]
# Para guardar un df sin índice: df.to_csv('archivo.csv', index=False)
# Para el conteo de valores distintos: c = len(df.col1.unique())