import pandas as pd
import datetime
from sqlalchemy import create_engine

# Conectar la base de datos del proyecto Moviva
# uri_con: URI de la Cenexión / engine: motor de conexión en SQLAlchemy
# uri_con = 'mssql+pyodbc://SA:Tamarilla1@35.193.216.104:1433/AnaliticaMoviva'
uri_con = 'mssql+pyodbc://Admin:Admin135*@35.193.96.39:1433/BDTAXPARAMETROS'
uri_con += '?driver=ODBC+Driver+17+for+SQL+Server'
engine = create_engine(uri_con, fast_executemany=True, connect_args={'connect_timeout': 10}, echo=False)

# Parametrizar las operaciones con el mes y año que se desean reentrenar
periodo, mes = 2021, 8

# Encontrar el primer dia del mes y del año recibidos y del mes siguiente
ini = datetime.date(periodo,mes,1)
fin = datetime.date(periodo,mes+1,1)

# Leer de la bd los feriados del mes y del año recibidos
# q: cadena con la sentecias SQL / f: DataFrame de feriados
q = "SELECT INTORDEN ORDEN,DTMFECHA FROM TBLDIAS_FESTIVOS "
q += "WHERE DTMFECHA >= '{}' AND DTMFECHA < '{}'".format(ini,fin)
f = pd.read_sql_query(q, engine)

# Leer de la bd los servicios del mes y del año recibidos
# s: DataFrame de servicios
q = """SELECT D.INTIDSERVICIODETALLE,CONVERT(DATE, D.DTMFECHAINGRESO) AS DIA,
    datepart(hh, D.DTMFECHAINGRESO) AS HORA, DR.INTCX, DR.INTCY
    FROM TBLSERVICIODETALLEHST D
    INNER JOIN TBLCLIENTE C ON D.INTIDCLIENTE=C.INTIDCLIENTE
    INNER JOIN TBLDIRECCIONCLIENTE DC ON C.INTIDCLIENTE=DC.INTIDCLIENTE
    INNER JOIN TBLDIRECCION DR ON DC.INTIDDIRECCION=DR.INTIDDIRECCION
    WHERE CONVERT(DATE, D.DTMFECHAINGRESO) >= '{}' AND CONVERT(DATE, D.DTMFECHAINGRESO) < '{}'
    GROUP BY D.INTIDSERVICIODETALLE,CONVERT(DATE, D.DTMFECHAINGRESO),datepart(hh,D.DTMFECHAINGRESO),
    DR.INTCX,DR.INTCY
    ORDER BY D.INTIDSERVICIODETALLE,CONVERT(DATE,D.DTMFECHAINGRESO),datepart(hh,D.DTMFECHAINGRESO)""".format(ini,fin)
s = pd.read_sql_query(q, engine)

# Filtrar las coordenadas para evitar servicios por fuera de la zona de influencia
s = s[(6.1173666 < s.INTCX) & (s.INTCX < 6.3617939)]
s = s[(-75.6659332 < s.INTCY) & (s.INTCY < -75.525264)]

# Preparar DataFrame de servicios para que quede similar al de las prediccicones
s['DIA'] = pd.to_datetime(s['DIA'])
s['SEMANA'] = pd.to_numeric(s.DIA.dt.strftime('%W'), downcast='integer')
s['DIA_NOMBRE'] = s.DIA.dt.strftime('%A')
s['LATITUD'] = round(s.INTCX,3)
s['LONGITUD'] = round(s.INTCY,3)
s.drop(['INTIDSERVICIODETALLE','INTCX','INTCY'], axis=1, inplace=True)

# Separar el DataFrame de servicios en dos: ordinarios y feriados
# ordi: DataFrame de ordinarios / feri: DataFrame de feriados
s['FERIADO'] = s.DIA.isin(f.DTMFECHA)
ordi = s[s.FERIADO==False]
feri_aux = s[s.FERIADO==True]
feri = feri_aux.merge(f, left_on='DIA', right_on='DTMFECHA')

# Agrupar para contar los servicios ordinarios en cada coordenada
# co: DataFrame con el conteo de servicios ordinarios
co = ordi.groupby(['SEMANA', 'DIA_NOMBRE', 'HORA', 'LATITUD', 'LONGITUD']).DIA.count()
co = pd.DataFrame(co)
co.reset_index(inplace=True)
co.rename(columns={'DIA':'CONTEO'}, inplace=True)

# Agrupar para contar los servicios feriados en cada coordenada
# cf: DataFrame con el conteo de servicios feriados
cf = feri.groupby(['ORDEN', 'HORA', 'LATITUD', 'LONGITUD']).DIA.count()
cf = pd.DataFrame(cf)
cf.reset_index(inplace=True)
cf.rename(columns={'DIA':'CONTEO'}, inplace=True)

# Crear tabla con semanas y días implicados en servicios ordinarios del mes en cuestión
# dio: DataFrame con los días implicados en los servicios ordinarios
dio = co.groupby(['SEMANA','DIA_NOMBRE'])['CONTEO'].nunique()
dio = pd.DataFrame(dio)
dio.reset_index(inplace=True)
dio.drop(['CONTEO'], axis=1, inplace=True)
dio.to_sql('TBLIMPLICADOS_ORDINARIOS', engine, if_exists='replace', index=False, chunksize=1000)

# Crear tabla con los días implicados en servicios feriados del mes en cuestión
# dif: DataFrame con los días implicados en los servicios feriados
dif = cf.groupby(['ORDEN'])['CONTEO'].nunique()
dif = pd.DataFrame(dif)
dif.reset_index(inplace=True)
dif.drop(['CONTEO'], axis=1, inplace=True)
dif.to_sql('TBLIMPLICADOS_FESTIVOS', engine, if_exists='replace', index=False, chunksize=1000)

# Leer de la bd las predicciones de días ordinarios implicados
# po: DataFrame con los promedios ordinarios
q = 'SELECT * FROM TBLPROMEDIOS_ORDINARIOS P '
q += 'WHERE EXISTS (SELECT 1 FROM TBLIMPLICADOS_ORDINARIOS S '
q += 'WHERE P.SEMANA = S.SEMANA AND P.DIA_NOMBRE = S.DIA_NOMBRE)'
po = pd.read_sql_query(q, engine)
po.drop(['INTSERVICIO_ORDINARIO'], axis=1, inplace=True)

# Leer de la bd las predicciones de días feriados implicados
# pf: DataFrame con los promedios feriados
q = 'SELECT * FROM TBLPROMEDIOS_FESTIVOS P '
q += 'WHERE EXISTS (SELECT 1 FROM TBLIMPLICADOS_FESTIVOS S '
q += 'WHERE P.ORDEN = S.ORDEN)'
pf = pd.read_sql_query(q, engine)
pf.drop(['INTSERVICIO_FESTIVO'], axis=1, inplace=True)

# Mezclar promedios y conteos ordinarios y hacer el suavizado exponencial
# poi: DataFrame con los promedios ordinarios implicados
poi = pd.merge(po, co, how='outer', on=['SEMANA','DIA_NOMBRE','HORA','LATITUD','LONGITUD'])
poi.fillna({'PROMEDIO': 0, 'CONTEO': 0}, inplace=True)
poi.PROMEDIO = poi.PROMEDIO * 0.8 + poi.CONTEO * 0.2
poi.drop(['CONTEO'], axis=1, inplace=True)

# Mezclar promedios y conteos feriados y hacer el suavizado exponencial
# pfi: DataFrame con los promedios feriados implicados
pfi = pd.merge(pf, cf, how='outer', on=['ORDEN', 'HORA', 'LATITUD', 'LONGITUD'])
pfi.fillna({'PROMEDIO': 0, 'CONTEO': 0}, inplace=True)
pfi.PROMEDIO = pfi.PROMEDIO * 0.8 + pfi.CONTEO * 0.2
pfi.drop(['CONTEO'], axis=1, inplace=True)

# Borrar datos previos e insertar las nuevas prediciones de días ordinarios implicados
q = 'DELETE FROM TBLPROMEDIOS_ORDINARIOS '
q += 'WHERE EXISTS (SELECT 1 FROM TBLIMPLICADOS_ORDINARIOS '
q += 'WHERE TBLPROMEDIOS_ORDINARIOS.SEMANA = TBLIMPLICADOS_ORDINARIOS.SEMANA '
q += 'AND TBLPROMEDIOS_ORDINARIOS.DIA_NOMBRE = TBLIMPLICADOS_ORDINARIOS.DIA_NOMBRE)'
#engine.execute(q)
#poi.to_sql("TBLPROMEDIOS_ORDINARIOS", engine, if_exists="append", index_label='INTSERVICIO_ORDINARIO', chunksize=100000)
print(len(poi))

# Borrar datos previos e insertar las nuevas prediciones de días festivos implicados
q = 'DELETE FROM TBLPROMEDIOS_FESTIVOS '
q += 'WHERE EXISTS (SELECT 1 FROM TBLIMPLICADOS_FESTIVOS '
q += 'WHERE TBLPROMEDIOS_FESTIVOS.ORDEN = TBLIMPLICADOS_FESTIVOS.ORDEN)'
#engine.execute(q)
#pfi.to_sql("TBLPROMEDIOS_FESTIVOS", engine, if_exists="append", index_label='INTSERVICIO_FESTIVO', chunksize=100000)
print(len(pfi))

engine.dispose()