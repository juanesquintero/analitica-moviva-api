import pandas as pd
import glob

# Cargar los datos provenientes del sistema Veridian
csv_files = glob.glob('data*.csv')
d = pd.DataFrame()
headers = ['id', 'fecha', 'hora', 'lat','lon'] 
dtypes = {'id': 'int', 'fecha': 'str', 'hora': 'str', 'lat': 'float', 'lon': 'float'}

for filename in csv_files:
  f = pd.read_csv(filename, header=None, names=headers, dtype=dtypes, parse_dates=['fecha','hora'])
  d = d.append(f)

# Formatear la hora para que quede compatible en los dos origenes de datos
d['hora'] = d.hora.dt.strftime('%H')

# Cargar los datos provenientes del sistema Moviva
file = 'datos_20190915_20210531-Moviva.csv'
f = pd.read_csv(file, header=None, names=headers, dtype=dtypes, parse_dates=['fecha'])
d = d.append(f)

# Filtrar las coordenadas para evitar servicios por fuera de la zona de influencia
d = d[(6.1173666 < d.lat) & (d.lat < 6.3617939)]
d = d[(-75.6659332 < d.lon) & (d.lon < -75.525264)]

# Filtrar las fechas para trabajar solo con servicios de 2017, 2018 y 2019
d = d[('2017-01-01' <= d.fecha) & (d.fecha <= '2019-12-31')]

# Redondear latitud y longitud para agrupar espacialmente los servicios cercanos
d.lat = round(d.lat,3)
d.lon = round(d.lon,3)

# Formatear los datos y generar nuevas columnas para su posterior tratamiento
d['periodo'] = d.fecha.dt.strftime('%Y')
d['semana'] = d.fecha.dt.strftime('%W')
d['dia_nombre'] = d.fecha.dt.strftime('%A')

# Agrupar los servicios de cada fecha, hora , latitud y longitud para su conteo
cd = d.groupby(['fecha','periodo','semana','dia_nombre','hora','lat','lon']).fecha.count()
cd.to_csv('Servicios Conteo X Hora con Fecha.csv',header=False)
