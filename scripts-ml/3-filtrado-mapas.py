import pandas as pd

d = pd.read_csv('Servicios Promedio X Hora - Ordinario.csv')

x = d[(d.semana==2) & (d.dia_nombre=='Tuesday') & (d.hora==6)]
x.to_excel('02-Mar (06-07).xlsx',merge_cells=False, index=False, float_format='%.3f')

x = d[(d.semana==13) & (d.dia_nombre=='Wednesday') & (d.hora==10)]
x.to_excel('13-Mie (10-11).xlsx',merge_cells=False, index=False, float_format='%.3f')

x = d[(d.semana==25) & (d.dia_nombre=='Thursday') & (d.hora==14)]
x.to_excel('25-Jue (14-15).xlsx',merge_cells=False, index=False, float_format='%.3f')

x = d[(d.semana==37) & (d.dia_nombre=='Friday') & (d.hora==18)]
x.to_excel('37-Vie (18-19).xlsx',merge_cells=False, index=False, float_format='%.3f')

x = d[(d.semana==50) & (d.dia_nombre=='Saturday') & (d.hora==22)]
x.to_excel('50-Sab (22-23).xlsx',merge_cells=False, index=False, float_format='%.3f')
