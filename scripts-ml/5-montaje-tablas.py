import pandas as pd
from sqlalchemy import create_engine

# Conectar la base de datos del proyecto Moviva
# con_uri = 'mssql+pyodbc://SA:Tamarilla1@35.193.216.104:1433/AnaliticaMoviva'
# con_uri = 'mssql+pyodbc://Admin:Admin135*@35.193.96.39:1433/BDTAXPARAMETROS'
con_uri = 'mssql+pyodbc://SA:MovivaPrd2020+@13.66.24.199:1433/BDTAXPARAMETROS'
con_uri += '?driver=ODBC+Driver+17+for+SQL+Server'
engine = create_engine(con_uri, fast_executemany=True, connect_args={'connect_timeout': 10}, echo=False)

# Montaje de la tabla de Feriados
file = 'feriados-colombia.csv'
headers = ['INTORDEN', 'STRNOMBRE', 'DTMFECHA', 'INTANNO', 'STRDESCRIPCION_DIA'] 
dtypes = {'INTORDEN': 'int', 'STRNOMBRE': 'str', 'DTMFECHA': 'str', 'INTANNO': 'int', 'STRDESCRIPCION_DIA': 'str'}
f = pd.read_csv(file, header=None, names=headers, dtype=dtypes, parse_dates=['DTMFECHA'],encoding="ISO-8859-1")
f.to_sql("TBLDIAS_FESTIVOS", engine, if_exists="replace", index_label='INTIDDIA_FESTIVO', chunksize=1000)

# Montaje de la tabla de Servicios Festivos
file = 'servicios-promedio-x-hora-feriados.csv'
headers = ['ORDEN', 'HORA', 'LATITUD', 'LONGITUD', 'PROMEDIO']
dtypes = {'ORDEN': 'int', 'HORA': 'int', 'LATITUD': 'float', 'LONGITUD': 'float', 'PROMEDIO': 'float'}
d = pd.read_csv(file, header=None, names=headers, dtype=dtypes, encoding="ISO-8859-1")
d.LATITUD = round(d.LATITUD,3)
d.LONGITUD = round(d.LONGITUD,3)
d.PROMEDIO = round(d.PROMEDIO,3)
d.to_sql("TBLPROMEDIOS_FESTIVOS", engine, if_exists="replace", index_label='INTSERVICIO_FESTIVO', chunksize=50000)

# Montaje de la tabla de Servicios Ordinarios
file = 'servicios-promedio-x-hora-ordinarios.csv'
headers = ['SEMANA', 'DIA_NOMBRE', 'HORA', 'LATITUD', 'LONGITUD', 'PROMEDIO']
dtypes = {'SEMANA': 'int', 'DIA_NOMBRE': 'str', 'HORA': 'int', 'LATITUD': 'float', 'LONGITUD': 'float', 'PROMEDIO': 'float'}
d = pd.read_csv(file, header=None, names=headers, dtype=dtypes, encoding="ISO-8859-1")
d.LATITUD = round(d.LATITUD,3)
d.LONGITUD = round(d.LONGITUD,3)
d.PROMEDIO = round(d.PROMEDIO,3)
rangos = [[0, 13], [14, 29], [30, 43], [44, 53]]
for i in range(len(rangos)):
     r = d[(rangos[i][0] <= d.SEMANA) & (d.SEMANA <= rangos[i][1])]
     r.to_sql("TBLPROMEDIOS_ORDINARIOS", engine, if_exists="append", index_label='INTSERVICIO_ORDINARIO', chunksize=50000)

engine.dispose()
