import pandas as pd
import datetime
from flask import jsonify, Blueprint
from api.db.connection import conectar
 
engine = conectar()
_periodo = None
_mes = None

PredictorServicios = Blueprint('PredictorServicios', __name__)

@PredictorServicios.route('/<int:periodo>/<int:mes>', methods=['POST'])
def predicir_cantidad_servicios(periodo, mes):

    global _periodo
    global _mes
    _periodo = periodo
    _mes = mes
    respuesta = {}

    # Encontrar el primer dia del mes y del año recibidos y del mes siguiente
    ini = datetime.date(periodo,mes,1)
    fin = datetime.date(periodo,mes+1,1)

    # Validar si el modleo ya esta entrenado para mes y del año recibidos
    # query: cadena con las sentecias SQL
    query = f'''SELECT 1 FROM TBLENTRENAMIENTOS WHERE INTMODELO_ANALITICA = 1 
        AND STRPERIODO = '{_periodo}-{_mes}' AND STRRESULTADO = 'Exitoso' '''
    entrenamientos = pd.read_sql_query(query, engine)

    if len(entrenamientos) > 0:
        mensaje = f'No se puede entrenar dos veces este modelo en el periodo {_periodo}-{_mes}.'
        respuesta = {'resultado': 'Fallido', 'mensaje': mensaje}
        escribir_resultados(respuesta)
        engine.dispose()
        raise ValueError(mensaje)

    # Leer de la bd los servicios del mes y del año recibidos
    # serv: DataFrame de servicios
    # TODO En desarrollo la tabla del FROM es TBLSERVICIODETALLE
    # TODO En produccion la tabla del FROM es TBLSERVICIODETALLEHST
    query = f'''SELECT D.INTIDSERVICIODETALLE,CONVERT(DATE, D.DTMFECHAINGRESO) 
        AS DIA,datepart(hh, D.DTMFECHAINGRESO) AS HORA, DR.INTCX, DR.INTCY
        FROM TBLSERVICIODETALLEHST D
        INNER JOIN TBLCLIENTE C ON D.INTIDCLIENTE=C.INTIDCLIENTE
        INNER JOIN TBLDIRECCIONCLIENTE DC ON C.INTIDCLIENTE=DC.INTIDCLIENTE
        INNER JOIN TBLDIRECCION DR ON DC.INTIDDIRECCION=DR.INTIDDIRECCION
        WHERE CONVERT(DATE, D.DTMFECHAINGRESO) >= '{ini}'
        AND CONVERT(DATE, D.DTMFECHAINGRESO) < '{fin}'
        GROUP BY D.INTIDSERVICIODETALLE,CONVERT(DATE, D.DTMFECHAINGRESO),
        datepart(hh,D.DTMFECHAINGRESO),DR.INTCX,DR.INTCY
        ORDER BY D.INTIDSERVICIODETALLE,CONVERT(DATE,D.DTMFECHAINGRESO),
        datepart(hh,D.DTMFECHAINGRESO)'''
    serv = leer_data_frame(query, 'Servicios')

    # Filtrar las coordenadas para evitar servicios por fuera de la zona de influencia
    serv = serv[(6.1173666 < serv.INTCX) & (serv.INTCX < 6.3617939)]
    serv = serv[(-75.6659332 < serv.INTCY) & (serv.INTCY < -75.525264)]

    # Preparar DataFrame de servicios para que quede similar al de las prediccicones
    serv['DIA'] = pd.to_datetime(serv['DIA'])
    serv['SEMANA'] = pd.to_numeric(serv.DIA.dt.strftime('%W'), downcast='integer')
    serv['DIA_NOMBRE'] = serv.DIA.dt.strftime('%A')
    serv['LATITUD'] = round(serv.INTCX,3)
    serv['LONGITUD'] = round(serv.INTCY,3)
    serv.drop(['INTIDSERVICIODETALLE','INTCX','INTCY'], axis=1, inplace=True)

    # Leer de la bd los feriados del mes y del año recibidos
    # dias_feri: DataFrame de feriados
    query = f'''SELECT INTORDEN ORDEN,DTMFECHA FROM TBLDIAS_FESTIVOS
        WHERE DTMFECHA >= '{ini}' AND DTMFECHA < '{fin}' '''
    dias_feri = pd.read_sql_query(query, engine)

    if dias_feri.empty:
        num_ord_generados = procesar_ordinarios(serv)
        respuesta['mensaje'] = f'Servicios ordinarios procesados: {len(serv)}'
        respuesta['mensaje'] += f', Predicciones ordinarias generadas: {num_ord_generados}'
        respuesta['resultado'] = 'Exitoso'
 
    else:
        # Separar el DataFrame de servicios en dos: ordinarios y feriados
        # ordi: DataFrame de ordinarios / feri: DataFrame de feriados
        serv['FERIADO'] = serv.DIA.isin(dias_feri.DTMFECHA)
        ordi = serv[serv.FERIADO == False]
        feri_aux = serv[serv.FERIADO == True]
        feri = feri_aux.merge(dias_feri, left_on='DIA', right_on='DTMFECHA')

        if len(ordi) > 0:
            num_ord_generados = procesar_ordinarios(ordi)
            respuesta['mensaje'] = f'Servicios ordinarios procesados: {len(ordi)}'
            respuesta['mensaje'] += f', Predicciones ordinarias generadas: {num_ord_generados}'

        if len(feri) > 0:
            num_fest_generados = procesar_festivos(feri)
            respuesta['mensaje'] += f', Servicios festivos procesados: {len(feri)}'
            respuesta['mensaje'] += f', Predicciones festivos generadas: {num_fest_generados}.'

    respuesta['resultado'] = 'Exitoso'
    escribir_resultados(respuesta)
    engine.dispose()
    return jsonify(respuesta), 200

def procesar_ordinarios(ordi):
    # Agrupar para contar los servicios ordinarios en cada coordenada
    # conteo_ord: DataFrame con el conteo de servicios ordinarios
    conteo_ord = ordi.groupby(['SEMANA', 'DIA_NOMBRE', 'HORA', 'LATITUD', 'LONGITUD']).DIA.count()
    conteo_ord = pd.DataFrame(conteo_ord)
    conteo_ord.reset_index(inplace=True)
    conteo_ord.rename(columns={'DIA':'CONTEO'}, inplace=True)

    # Crear tabla con semanas y días implicados en servicios ordinarios del mes en cuestión
    # dias_ord: DataFrame con los días implicados en los servicios ordinarios
    dias_ord = conteo_ord.groupby(['SEMANA','DIA_NOMBRE'])['CONTEO'].nunique()
    dias_ord = pd.DataFrame(dias_ord)
    dias_ord.reset_index(inplace=True)
    dias_ord.drop(['CONTEO'], axis=1, inplace=True)
    dias_ord.to_sql('TBLIMPLICADOS_ORDINARIOS', engine, if_exists='replace', index=False, chunksize=1000)

    # Leer de la bd las predicciones de días ordinarios implicados
    # prom_ord: DataFrame con los promedios ordinarios
    query = '''SELECT * FROM TBLPROMEDIOS_ORDINARIOS P
        WHERE EXISTS (SELECT 1 FROM TBLIMPLICADOS_ORDINARIOS S
        WHERE P.SEMANA = S.SEMANA AND P.DIA_NOMBRE = S.DIA_NOMBRE)'''
    prom_ord = leer_data_frame(query, 'Predicciones para Días Ordinarios')
    prom_ord.drop(['INTSERVICIO_ORDINARIO'], axis=1, inplace=True)

    # Mezclar promedios y conteos ordinarios y hacer el suavizado exponencial
    # prom_impl: DataFrame con los promedios ordinarios implicados
    prom_impl = pd.merge(prom_ord, conteo_ord, how='outer', on=['SEMANA','DIA_NOMBRE','HORA','LATITUD','LONGITUD'])
    prom_impl.fillna({'PROMEDIO': 0, 'CONTEO': 0}, inplace=True)
    prom_impl.PROMEDIO = prom_impl.PROMEDIO * 0.8 + prom_impl.CONTEO * 0.2
    prom_impl.drop(['CONTEO'], axis=1, inplace=True)

    # Borrar datos previos e insertar las nuevas prediciones de días ordinarios implicados
    query = '''DELETE FROM TBLPROMEDIOS_ORDINARIOS
        WHERE EXISTS (SELECT 1 FROM TBLIMPLICADOS_ORDINARIOS
        WHERE TBLPROMEDIOS_ORDINARIOS.SEMANA = TBLIMPLICADOS_ORDINARIOS.SEMANA
        AND TBLPROMEDIOS_ORDINARIOS.DIA_NOMBRE = TBLIMPLICADOS_ORDINARIOS.DIA_NOMBRE)'''
    engine.execute(query)
    #prom_impl.to_sql("TBLPROMEDIOS_ORDINARIOS", engine, if_exists="append", index_label='INTSERVICIO_ORDINARIO', chunksize=50000)
    escribir_df_grande(prom_impl, 'TBLPROMEDIOS_ORDINARIOS', 'INTSERVICIO_ORDINARIO', 100000)
    return len(prom_impl)

def procesar_festivos(feri):
    # Agrupar para contar los servicios feriados en cada coordenada
    # conteo_fest: DataFrame con el conteo de servicios feriados
    conteo_fest = feri.groupby(['ORDEN', 'HORA', 'LATITUD', 'LONGITUD']).DIA.count()
    conteo_fest = pd.DataFrame(conteo_fest)
    conteo_fest.reset_index(inplace=True)
    conteo_fest.rename(columns={'DIA':'CONTEO'}, inplace=True)

    # Crear tabla con los días implicados en servicios feriados del mes en cuestión
    # dias_fest: DataFrame con los días implicados en los servicios feriados
    dias_fest = conteo_fest.groupby(['ORDEN'])['CONTEO'].nunique()
    dias_fest = pd.DataFrame(dias_fest)
    dias_fest.reset_index(inplace=True)
    dias_fest.drop(['CONTEO'], axis=1, inplace=True)
    dias_fest.to_sql('TBLIMPLICADOS_FESTIVOS', engine, if_exists='replace', index=False, chunksize=1000)

    # Leer de la bd las predicciones de días feriados implicados
    # prom_fest: DataFrame con los promedios feriados
    query = '''SELECT * FROM TBLPROMEDIOS_FESTIVOS P
        WHERE EXISTS (SELECT 1 FROM TBLIMPLICADOS_FESTIVOS S
        WHERE P.ORDEN = S.ORDEN)'''
    prom_fest = leer_data_frame(query, 'Predicciones para Días Festivos')
    prom_fest.drop(['INTSERVICIO_FESTIVO'], axis=1, inplace=True)

    # Mezclar promedios y conteos feriados y hacer el suavizado exponencial
    # prom_impl: DataFrame con los promedios feriados implicados
    prom_impl = pd.merge(prom_fest, conteo_fest, how='outer', on=['ORDEN', 'HORA', 'LATITUD', 'LONGITUD'])
    prom_impl.fillna({'PROMEDIO': 0, 'CONTEO': 0}, inplace=True)
    prom_impl.PROMEDIO = prom_impl.PROMEDIO * 0.8 + prom_impl.CONTEO * 0.2
    prom_impl.drop(['CONTEO'], axis=1, inplace=True)

    # Borrar datos previos e insertar las nuevas prediciones de días festivos implicados
    query = '''DELETE FROM TBLPROMEDIOS_FESTIVOS
        WHERE EXISTS (SELECT 1 FROM TBLIMPLICADOS_FESTIVOS
        WHERE TBLPROMEDIOS_FESTIVOS.ORDEN = TBLIMPLICADOS_FESTIVOS.ORDEN)'''
    engine.execute(query)
    #prom_impl.to_sql("TBLPROMEDIOS_FESTIVOS", engine, if_exists="append", index_label='INTSERVICIO_FESTIVO', chunksize=50000)
    escribir_df_grande(prom_impl, "TBLPROMEDIOS_FESTIVOS", 'INTSERVICIO_FESTIVO', 100000)
    return len(prom_impl)

def leer_data_frame(query, entidad):
    d = pd.read_sql_query(query, engine)
    if d.empty:
        mensaje = f'No se encontraron registros de {entidad}.'
        respuesta = {'resultado': 'Fallido', 'mensaje': mensaje}
        escribir_resultados(respuesta)
        engine.dispose()
        raise ValueError(mensaje)
    return d

def escribir_resultados(respuesta):
    global _periodo
    global _mes
    data = pd.DataFrame({
        'INTMODELO_ANALITICA':[1],
        'STRPERIODO':[f'{_periodo}-{_mes}'],
        'DTMFECHA':[datetime.datetime.now()],
        'STRRESULTADO':[respuesta['resultado']],
        'STRMENSAJE':[respuesta['mensaje']],
        'STROBSERVACIONES':[None]
    })
    data.to_sql('TBLENTRENAMIENTOS', engine, if_exists='append', index_label='INTENTRENAMIENTO', chunksize=1000)

def escribir_df_grande(df, table, index, chunk_size_param):
    start_index = 0
    end_index = min(chunk_size_param, len(df))
    while start_index != end_index:
        df.iloc[start_index:end_index, :].to_sql(table, engine, if_exists='append', index_label=index)
        start_index = min(start_index + chunk_size_param, len(df))
        end_index = min(end_index + chunk_size_param, len(df))
