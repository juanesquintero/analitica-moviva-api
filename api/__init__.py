from flask import Flask, jsonify, request
from flask_cors import CORS
from api.routes.controller import PredictorServicios

def create_api():
    # Inicializar Aplicacion de Flask
    api = Flask(__name__)
    CORS(api)

    with api.app_context():
        # Configuracion base de app
        config(api)

        # Registrar rutas con vistas del api
        routes(api)

        # Manejador/Captura de errores
        error_handlers(api)

    return api

def config(api):
    # Definir variables de configuracion (desde config.py)
    api.config.from_object('config')

def routes(api):
    root_path = api.config.get('ROOT_PATH')

    @api.route(root_path)
    @api.route(root_path+'/')
    def index():
        return jsonify({'api': 'Analítica Moviva'}), 200

    # Register routes
    api.register_blueprint(PredictorServicios, url_prefix=f'{root_path}/predictor-servicios')

def error_handlers(api):
    error_logger = api.config['ERROR_LOGGER']

    @api.errorhandler(404)
    def page_not_found(e):
        return jsonify({'resultado': 'Fallido', 'mensaje': 'Endpoint No Encontrado.'}), 404

    @api.errorhandler(405)
    def method_not_allow(e):
        return jsonify({'resultado': 'Fallido', 'mensaje': 'Método No Permitido.'}), 405

    @api.errorhandler(500)
    def handle_500(e):
        error_logger.error(e)
        return jsonify({'resultado': 'Fallido', 'mensaje': 'Error del Servidor del Sistema.'}), 500

    @api.errorhandler(Exception)
    def handle_exception(e):
        error_logger.error('EXCEPTION: '+str(e))
        return jsonify({'resultado': 'Fallido', 'mensaje': str(e)}), 500
