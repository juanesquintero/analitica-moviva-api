import os
from sqlalchemy import create_engine
from dotenv import load_dotenv

load_dotenv()

_host = os.getenv('MOVIVA_HOST')
_port = os.getenv('MOVIVA_PORT')
_user = os.getenv("MOVIVA_USER")
_password = os.getenv("MOVIVA_PASSWORD")
_database = os.getenv('MOVIVA_DATABASE')

def conectar():
    '''
    Conectar la base de datos del proyecto Moviva\n
    uri_con: URI de la Cenexión / engine: motor de conexión en SQLAlchemy
    '''
    uri_con = f'mssql+pyodbc://{_user}:{_password}@{_host}:{_port}/{_database}'
    uri_con += '?driver=ODBC+Driver+17+for+SQL+Server'
    engine = create_engine(uri_con, fast_executemany=True, echo=False)
    return engine
