import secrets
from random import randint

from app.forms.auth_forms import RegisterForm, LoginForm

random_name = secrets.token_hex(nbytes=randint(5, 15))

mock_test_register_user = dict(
    name='Pepe '+random_name,
    username=random_name,
    password=random_name,
    confirm_password=random_name
)

mock_test_login_user = dict(
    username=random_name,
    password=random_name,
)

product_keys = ['product_name', 'product_price', 'product_warranty']

def mock_register_form(app):
    with app.test_request_context():
        form = RegisterForm(**mock_test_register_user)
    return form

def mock_login_form(app):
    with app.test_request_context():
        form = LoginForm(**mock_test_login_user)
    return form
