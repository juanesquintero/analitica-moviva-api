'''
The main config file for Superset

All configuration in this file can be overridden by providing a superset_config
in your PYTHONPATH as there is a ``from superset_config import *``
at the end of this file.
'''
import os
import logging
import datetime
from dotenv import load_dotenv

load_dotenv()

base_dir = os.path.dirname(os.path.abspath(__file__))

JSON_SORT_KEYS = False
ROOT_PATH = '/api/v1'

# Reloadn on Jinja templates change
TEMPLATES_AUTO_RELOAD = True
DEBUG_TB_ENABLED = False

# Session Config
SECRET_KEY = os.environ.get('ABCREP_SECRET_KEY')
SESSION_PERMANENT = True
SESSION_TYPE = 'filesystem'
PERMANENT_SESSION_LIFETIME = datetime.timedelta(minutes=60)

'''LOGGING CONFIG'''
LOG_FORMAT = '%(asctime)s:%(levelname)s:%(name)s:%(message)s'
LOG_DIR = base_dir + '/logs'

# # GENERAL LOGS (ALL)
# logging.getLogger().setLevel(logging.DEBUG)
# logging.basicConfig(filename=LOG_DIR+'/GENERALS.log',level=logging.DEBUG,format=LOG_FORMAT)

# ERROR LOGS
error_logger = logging.getLogger('error_logger')
error_logger.setLevel(logging.ERROR)
file_handler = logging.FileHandler(LOG_DIR+'/ERRORS.log')
file_handler.setFormatter(logging.Formatter(LOG_FORMAT))
error_logger.addHandler(file_handler)
'''END LOGGING CONFIG'''

ERROR_LOGGER = logging.getLogger('error_logger')

''' DOMAIN CONFIG VARIABLES '''
# OAuth2 config
OAUTH_CLIENT_ID = os.environ.get('OAUTH_CLIENT_ID')
OAUTH_CLIENT_SECRET = os.environ.get('OAUTH_CLIENT_SECRET')
ACCESS_TOKEN_URL = 'oauth/token'
